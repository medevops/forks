// Code generated by protoc-gen-go. DO NOT EDIT.
// versions:
// 	protoc-gen-go v1.28.0
// 	protoc        v3.21.12
// source: forks.proto

package forks

import (
	protoreflect "google.golang.org/protobuf/reflect/protoreflect"
	protoimpl "google.golang.org/protobuf/runtime/protoimpl"
	reflect "reflect"
	sync "sync"
)

const (
	// Verify that this generated code is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(20 - protoimpl.MinVersion)
	// Verify that runtime/protoimpl is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(protoimpl.MaxVersion - 20)
)

type StoreForkRequest struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Forks *Fork `protobuf:"bytes,1,opt,name=Forks,proto3" json:"Forks,omitempty"`
}

func (x *StoreForkRequest) Reset() {
	*x = StoreForkRequest{}
	if protoimpl.UnsafeEnabled {
		mi := &file_forks_proto_msgTypes[0]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *StoreForkRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*StoreForkRequest) ProtoMessage() {}

func (x *StoreForkRequest) ProtoReflect() protoreflect.Message {
	mi := &file_forks_proto_msgTypes[0]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use StoreForkRequest.ProtoReflect.Descriptor instead.
func (*StoreForkRequest) Descriptor() ([]byte, []int) {
	return file_forks_proto_rawDescGZIP(), []int{0}
}

func (x *StoreForkRequest) GetForks() *Fork {
	if x != nil {
		return x.Forks
	}
	return nil
}

type ForksResponse struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Status string `protobuf:"bytes,1,opt,name=Status,proto3" json:"Status,omitempty"`
}

func (x *ForksResponse) Reset() {
	*x = ForksResponse{}
	if protoimpl.UnsafeEnabled {
		mi := &file_forks_proto_msgTypes[1]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *ForksResponse) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*ForksResponse) ProtoMessage() {}

func (x *ForksResponse) ProtoReflect() protoreflect.Message {
	mi := &file_forks_proto_msgTypes[1]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use ForksResponse.ProtoReflect.Descriptor instead.
func (*ForksResponse) Descriptor() ([]byte, []int) {
	return file_forks_proto_rawDescGZIP(), []int{1}
}

func (x *ForksResponse) GetStatus() string {
	if x != nil {
		return x.Status
	}
	return ""
}

type SubscribeRequest struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Key string `protobuf:"bytes,1,opt,name=Key,proto3" json:"Key,omitempty"`
}

func (x *SubscribeRequest) Reset() {
	*x = SubscribeRequest{}
	if protoimpl.UnsafeEnabled {
		mi := &file_forks_proto_msgTypes[2]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *SubscribeRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*SubscribeRequest) ProtoMessage() {}

func (x *SubscribeRequest) ProtoReflect() protoreflect.Message {
	mi := &file_forks_proto_msgTypes[2]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use SubscribeRequest.ProtoReflect.Descriptor instead.
func (*SubscribeRequest) Descriptor() ([]byte, []int) {
	return file_forks_proto_rawDescGZIP(), []int{2}
}

func (x *SubscribeRequest) GetKey() string {
	if x != nil {
		return x.Key
	}
	return ""
}

type DistributedForks struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Forks []*Fork `protobuf:"bytes,1,rep,name=Forks,proto3" json:"Forks,omitempty"`
}

func (x *DistributedForks) Reset() {
	*x = DistributedForks{}
	if protoimpl.UnsafeEnabled {
		mi := &file_forks_proto_msgTypes[3]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *DistributedForks) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*DistributedForks) ProtoMessage() {}

func (x *DistributedForks) ProtoReflect() protoreflect.Message {
	mi := &file_forks_proto_msgTypes[3]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use DistributedForks.ProtoReflect.Descriptor instead.
func (*DistributedForks) Descriptor() ([]byte, []int) {
	return file_forks_proto_rawDescGZIP(), []int{3}
}

func (x *DistributedForks) GetForks() []*Fork {
	if x != nil {
		return x.Forks
	}
	return nil
}

type Fork struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	EventId        string    `protobuf:"bytes,1,opt,name=EventId,proto3" json:"EventId,omitempty"`
	ForkId         string    `protobuf:"bytes,2,opt,name=ForkId,proto3" json:"ForkId,omitempty"`
	Sport          string    `protobuf:"bytes,3,opt,name=Sport,proto3" json:"Sport,omitempty"`
	BookmakersPair int64     `protobuf:"varint,4,opt,name=BookmakersPair,proto3" json:"BookmakersPair,omitempty"`
	Profit         float32   `protobuf:"fixed32,5,opt,name=Profit,proto3" json:"Profit,omitempty"`
	Created        string    `protobuf:"bytes,6,opt,name=Created,proto3" json:"Created,omitempty"`
	LastUpdated    string    `protobuf:"bytes,7,opt,name=LastUpdated,proto3" json:"LastUpdated,omitempty"`
	Start          string    `protobuf:"bytes,8,opt,name=Start,proto3" json:"Start,omitempty"`
	FirstShoulder  *Shoulder `protobuf:"bytes,9,opt,name=FirstShoulder,proto3" json:"FirstShoulder,omitempty"`
	SecondShoulder *Shoulder `protobuf:"bytes,10,opt,name=SecondShoulder,proto3" json:"SecondShoulder,omitempty"`
}

func (x *Fork) Reset() {
	*x = Fork{}
	if protoimpl.UnsafeEnabled {
		mi := &file_forks_proto_msgTypes[4]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *Fork) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*Fork) ProtoMessage() {}

func (x *Fork) ProtoReflect() protoreflect.Message {
	mi := &file_forks_proto_msgTypes[4]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use Fork.ProtoReflect.Descriptor instead.
func (*Fork) Descriptor() ([]byte, []int) {
	return file_forks_proto_rawDescGZIP(), []int{4}
}

func (x *Fork) GetEventId() string {
	if x != nil {
		return x.EventId
	}
	return ""
}

func (x *Fork) GetForkId() string {
	if x != nil {
		return x.ForkId
	}
	return ""
}

func (x *Fork) GetSport() string {
	if x != nil {
		return x.Sport
	}
	return ""
}

func (x *Fork) GetBookmakersPair() int64 {
	if x != nil {
		return x.BookmakersPair
	}
	return 0
}

func (x *Fork) GetProfit() float32 {
	if x != nil {
		return x.Profit
	}
	return 0
}

func (x *Fork) GetCreated() string {
	if x != nil {
		return x.Created
	}
	return ""
}

func (x *Fork) GetLastUpdated() string {
	if x != nil {
		return x.LastUpdated
	}
	return ""
}

func (x *Fork) GetStart() string {
	if x != nil {
		return x.Start
	}
	return ""
}

func (x *Fork) GetFirstShoulder() *Shoulder {
	if x != nil {
		return x.FirstShoulder
	}
	return nil
}

func (x *Fork) GetSecondShoulder() *Shoulder {
	if x != nil {
		return x.SecondShoulder
	}
	return nil
}

type Shoulder struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Bookmaker   string `protobuf:"bytes,1,opt,name=Bookmaker,proto3" json:"Bookmaker,omitempty"`
	ShoulderId  string `protobuf:"bytes,2,opt,name=ShoulderId,proto3" json:"ShoulderId,omitempty"`
	Team1       string `protobuf:"bytes,3,opt,name=Team1,proto3" json:"Team1,omitempty"`
	Team2       string `protobuf:"bytes,4,opt,name=Team2,proto3" json:"Team2,omitempty"`
	Period      string `protobuf:"bytes,5,opt,name=Period,proto3" json:"Period,omitempty"`
	Url         string `protobuf:"bytes,6,opt,name=Url,proto3" json:"Url,omitempty"`
	IsBreak     bool   `protobuf:"varint,7,opt,name=IsBreak,proto3" json:"IsBreak,omitempty"`
	Duration    int32  `protobuf:"varint,8,opt,name=Duration,proto3" json:"Duration,omitempty"`
	Score       string `protobuf:"bytes,9,opt,name=Score,proto3" json:"Score,omitempty"`
	Liga        string `protobuf:"bytes,10,opt,name=Liga,proto3" json:"Liga,omitempty"`
	IsInitiator bool   `protobuf:"varint,11,opt,name=IsInitiator,proto3" json:"IsInitiator,omitempty"`
	OddInfo     *Odd   `protobuf:"bytes,12,opt,name=OddInfo,proto3" json:"OddInfo,omitempty"`
}

func (x *Shoulder) Reset() {
	*x = Shoulder{}
	if protoimpl.UnsafeEnabled {
		mi := &file_forks_proto_msgTypes[5]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *Shoulder) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*Shoulder) ProtoMessage() {}

func (x *Shoulder) ProtoReflect() protoreflect.Message {
	mi := &file_forks_proto_msgTypes[5]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use Shoulder.ProtoReflect.Descriptor instead.
func (*Shoulder) Descriptor() ([]byte, []int) {
	return file_forks_proto_rawDescGZIP(), []int{5}
}

func (x *Shoulder) GetBookmaker() string {
	if x != nil {
		return x.Bookmaker
	}
	return ""
}

func (x *Shoulder) GetShoulderId() string {
	if x != nil {
		return x.ShoulderId
	}
	return ""
}

func (x *Shoulder) GetTeam1() string {
	if x != nil {
		return x.Team1
	}
	return ""
}

func (x *Shoulder) GetTeam2() string {
	if x != nil {
		return x.Team2
	}
	return ""
}

func (x *Shoulder) GetPeriod() string {
	if x != nil {
		return x.Period
	}
	return ""
}

func (x *Shoulder) GetUrl() string {
	if x != nil {
		return x.Url
	}
	return ""
}

func (x *Shoulder) GetIsBreak() bool {
	if x != nil {
		return x.IsBreak
	}
	return false
}

func (x *Shoulder) GetDuration() int32 {
	if x != nil {
		return x.Duration
	}
	return 0
}

func (x *Shoulder) GetScore() string {
	if x != nil {
		return x.Score
	}
	return ""
}

func (x *Shoulder) GetLiga() string {
	if x != nil {
		return x.Liga
	}
	return ""
}

func (x *Shoulder) GetIsInitiator() bool {
	if x != nil {
		return x.IsInitiator
	}
	return false
}

func (x *Shoulder) GetOddInfo() *Odd {
	if x != nil {
		return x.OddInfo
	}
	return nil
}

type Odd struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Type     string  `protobuf:"bytes,1,opt,name=Type,proto3" json:"Type,omitempty"`
	Mod      string  `protobuf:"bytes,2,opt,name=Mod,proto3" json:"Mod,omitempty"`
	Param    float32 `protobuf:"fixed32,3,opt,name=Param,proto3" json:"Param,omitempty"`
	Coef     float32 `protobuf:"fixed32,4,opt,name=Coef,proto3" json:"Coef,omitempty"`
	Selector string  `protobuf:"bytes,5,opt,name=Selector,proto3" json:"Selector,omitempty"`
}

func (x *Odd) Reset() {
	*x = Odd{}
	if protoimpl.UnsafeEnabled {
		mi := &file_forks_proto_msgTypes[6]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *Odd) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*Odd) ProtoMessage() {}

func (x *Odd) ProtoReflect() protoreflect.Message {
	mi := &file_forks_proto_msgTypes[6]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use Odd.ProtoReflect.Descriptor instead.
func (*Odd) Descriptor() ([]byte, []int) {
	return file_forks_proto_rawDescGZIP(), []int{6}
}

func (x *Odd) GetType() string {
	if x != nil {
		return x.Type
	}
	return ""
}

func (x *Odd) GetMod() string {
	if x != nil {
		return x.Mod
	}
	return ""
}

func (x *Odd) GetParam() float32 {
	if x != nil {
		return x.Param
	}
	return 0
}

func (x *Odd) GetCoef() float32 {
	if x != nil {
		return x.Coef
	}
	return 0
}

func (x *Odd) GetSelector() string {
	if x != nil {
		return x.Selector
	}
	return ""
}

var File_forks_proto protoreflect.FileDescriptor

var file_forks_proto_rawDesc = []byte{
	0x0a, 0x0b, 0x66, 0x6f, 0x72, 0x6b, 0x73, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x12, 0x05, 0x66,
	0x6f, 0x72, 0x6b, 0x73, 0x22, 0x35, 0x0a, 0x10, 0x53, 0x74, 0x6f, 0x72, 0x65, 0x46, 0x6f, 0x72,
	0x6b, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x12, 0x21, 0x0a, 0x05, 0x46, 0x6f, 0x72, 0x6b,
	0x73, 0x18, 0x01, 0x20, 0x01, 0x28, 0x0b, 0x32, 0x0b, 0x2e, 0x66, 0x6f, 0x72, 0x6b, 0x73, 0x2e,
	0x46, 0x6f, 0x72, 0x6b, 0x52, 0x05, 0x46, 0x6f, 0x72, 0x6b, 0x73, 0x22, 0x27, 0x0a, 0x0d, 0x46,
	0x6f, 0x72, 0x6b, 0x73, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x12, 0x16, 0x0a, 0x06,
	0x53, 0x74, 0x61, 0x74, 0x75, 0x73, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09, 0x52, 0x06, 0x53, 0x74,
	0x61, 0x74, 0x75, 0x73, 0x22, 0x24, 0x0a, 0x10, 0x53, 0x75, 0x62, 0x73, 0x63, 0x72, 0x69, 0x62,
	0x65, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x12, 0x10, 0x0a, 0x03, 0x4b, 0x65, 0x79, 0x18,
	0x01, 0x20, 0x01, 0x28, 0x09, 0x52, 0x03, 0x4b, 0x65, 0x79, 0x22, 0x35, 0x0a, 0x10, 0x44, 0x69,
	0x73, 0x74, 0x72, 0x69, 0x62, 0x75, 0x74, 0x65, 0x64, 0x46, 0x6f, 0x72, 0x6b, 0x73, 0x12, 0x21,
	0x0a, 0x05, 0x46, 0x6f, 0x72, 0x6b, 0x73, 0x18, 0x01, 0x20, 0x03, 0x28, 0x0b, 0x32, 0x0b, 0x2e,
	0x66, 0x6f, 0x72, 0x6b, 0x73, 0x2e, 0x46, 0x6f, 0x72, 0x6b, 0x52, 0x05, 0x46, 0x6f, 0x72, 0x6b,
	0x73, 0x22, 0xd0, 0x02, 0x0a, 0x04, 0x46, 0x6f, 0x72, 0x6b, 0x12, 0x18, 0x0a, 0x07, 0x45, 0x76,
	0x65, 0x6e, 0x74, 0x49, 0x64, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09, 0x52, 0x07, 0x45, 0x76, 0x65,
	0x6e, 0x74, 0x49, 0x64, 0x12, 0x16, 0x0a, 0x06, 0x46, 0x6f, 0x72, 0x6b, 0x49, 0x64, 0x18, 0x02,
	0x20, 0x01, 0x28, 0x09, 0x52, 0x06, 0x46, 0x6f, 0x72, 0x6b, 0x49, 0x64, 0x12, 0x14, 0x0a, 0x05,
	0x53, 0x70, 0x6f, 0x72, 0x74, 0x18, 0x03, 0x20, 0x01, 0x28, 0x09, 0x52, 0x05, 0x53, 0x70, 0x6f,
	0x72, 0x74, 0x12, 0x26, 0x0a, 0x0e, 0x42, 0x6f, 0x6f, 0x6b, 0x6d, 0x61, 0x6b, 0x65, 0x72, 0x73,
	0x50, 0x61, 0x69, 0x72, 0x18, 0x04, 0x20, 0x01, 0x28, 0x03, 0x52, 0x0e, 0x42, 0x6f, 0x6f, 0x6b,
	0x6d, 0x61, 0x6b, 0x65, 0x72, 0x73, 0x50, 0x61, 0x69, 0x72, 0x12, 0x16, 0x0a, 0x06, 0x50, 0x72,
	0x6f, 0x66, 0x69, 0x74, 0x18, 0x05, 0x20, 0x01, 0x28, 0x02, 0x52, 0x06, 0x50, 0x72, 0x6f, 0x66,
	0x69, 0x74, 0x12, 0x18, 0x0a, 0x07, 0x43, 0x72, 0x65, 0x61, 0x74, 0x65, 0x64, 0x18, 0x06, 0x20,
	0x01, 0x28, 0x09, 0x52, 0x07, 0x43, 0x72, 0x65, 0x61, 0x74, 0x65, 0x64, 0x12, 0x20, 0x0a, 0x0b,
	0x4c, 0x61, 0x73, 0x74, 0x55, 0x70, 0x64, 0x61, 0x74, 0x65, 0x64, 0x18, 0x07, 0x20, 0x01, 0x28,
	0x09, 0x52, 0x0b, 0x4c, 0x61, 0x73, 0x74, 0x55, 0x70, 0x64, 0x61, 0x74, 0x65, 0x64, 0x12, 0x14,
	0x0a, 0x05, 0x53, 0x74, 0x61, 0x72, 0x74, 0x18, 0x08, 0x20, 0x01, 0x28, 0x09, 0x52, 0x05, 0x53,
	0x74, 0x61, 0x72, 0x74, 0x12, 0x35, 0x0a, 0x0d, 0x46, 0x69, 0x72, 0x73, 0x74, 0x53, 0x68, 0x6f,
	0x75, 0x6c, 0x64, 0x65, 0x72, 0x18, 0x09, 0x20, 0x01, 0x28, 0x0b, 0x32, 0x0f, 0x2e, 0x66, 0x6f,
	0x72, 0x6b, 0x73, 0x2e, 0x53, 0x68, 0x6f, 0x75, 0x6c, 0x64, 0x65, 0x72, 0x52, 0x0d, 0x46, 0x69,
	0x72, 0x73, 0x74, 0x53, 0x68, 0x6f, 0x75, 0x6c, 0x64, 0x65, 0x72, 0x12, 0x37, 0x0a, 0x0e, 0x53,
	0x65, 0x63, 0x6f, 0x6e, 0x64, 0x53, 0x68, 0x6f, 0x75, 0x6c, 0x64, 0x65, 0x72, 0x18, 0x0a, 0x20,
	0x01, 0x28, 0x0b, 0x32, 0x0f, 0x2e, 0x66, 0x6f, 0x72, 0x6b, 0x73, 0x2e, 0x53, 0x68, 0x6f, 0x75,
	0x6c, 0x64, 0x65, 0x72, 0x52, 0x0e, 0x53, 0x65, 0x63, 0x6f, 0x6e, 0x64, 0x53, 0x68, 0x6f, 0x75,
	0x6c, 0x64, 0x65, 0x72, 0x22, 0xc6, 0x02, 0x0a, 0x08, 0x53, 0x68, 0x6f, 0x75, 0x6c, 0x64, 0x65,
	0x72, 0x12, 0x1c, 0x0a, 0x09, 0x42, 0x6f, 0x6f, 0x6b, 0x6d, 0x61, 0x6b, 0x65, 0x72, 0x18, 0x01,
	0x20, 0x01, 0x28, 0x09, 0x52, 0x09, 0x42, 0x6f, 0x6f, 0x6b, 0x6d, 0x61, 0x6b, 0x65, 0x72, 0x12,
	0x1e, 0x0a, 0x0a, 0x53, 0x68, 0x6f, 0x75, 0x6c, 0x64, 0x65, 0x72, 0x49, 0x64, 0x18, 0x02, 0x20,
	0x01, 0x28, 0x09, 0x52, 0x0a, 0x53, 0x68, 0x6f, 0x75, 0x6c, 0x64, 0x65, 0x72, 0x49, 0x64, 0x12,
	0x14, 0x0a, 0x05, 0x54, 0x65, 0x61, 0x6d, 0x31, 0x18, 0x03, 0x20, 0x01, 0x28, 0x09, 0x52, 0x05,
	0x54, 0x65, 0x61, 0x6d, 0x31, 0x12, 0x14, 0x0a, 0x05, 0x54, 0x65, 0x61, 0x6d, 0x32, 0x18, 0x04,
	0x20, 0x01, 0x28, 0x09, 0x52, 0x05, 0x54, 0x65, 0x61, 0x6d, 0x32, 0x12, 0x16, 0x0a, 0x06, 0x50,
	0x65, 0x72, 0x69, 0x6f, 0x64, 0x18, 0x05, 0x20, 0x01, 0x28, 0x09, 0x52, 0x06, 0x50, 0x65, 0x72,
	0x69, 0x6f, 0x64, 0x12, 0x10, 0x0a, 0x03, 0x55, 0x72, 0x6c, 0x18, 0x06, 0x20, 0x01, 0x28, 0x09,
	0x52, 0x03, 0x55, 0x72, 0x6c, 0x12, 0x18, 0x0a, 0x07, 0x49, 0x73, 0x42, 0x72, 0x65, 0x61, 0x6b,
	0x18, 0x07, 0x20, 0x01, 0x28, 0x08, 0x52, 0x07, 0x49, 0x73, 0x42, 0x72, 0x65, 0x61, 0x6b, 0x12,
	0x1a, 0x0a, 0x08, 0x44, 0x75, 0x72, 0x61, 0x74, 0x69, 0x6f, 0x6e, 0x18, 0x08, 0x20, 0x01, 0x28,
	0x05, 0x52, 0x08, 0x44, 0x75, 0x72, 0x61, 0x74, 0x69, 0x6f, 0x6e, 0x12, 0x14, 0x0a, 0x05, 0x53,
	0x63, 0x6f, 0x72, 0x65, 0x18, 0x09, 0x20, 0x01, 0x28, 0x09, 0x52, 0x05, 0x53, 0x63, 0x6f, 0x72,
	0x65, 0x12, 0x12, 0x0a, 0x04, 0x4c, 0x69, 0x67, 0x61, 0x18, 0x0a, 0x20, 0x01, 0x28, 0x09, 0x52,
	0x04, 0x4c, 0x69, 0x67, 0x61, 0x12, 0x20, 0x0a, 0x0b, 0x49, 0x73, 0x49, 0x6e, 0x69, 0x74, 0x69,
	0x61, 0x74, 0x6f, 0x72, 0x18, 0x0b, 0x20, 0x01, 0x28, 0x08, 0x52, 0x0b, 0x49, 0x73, 0x49, 0x6e,
	0x69, 0x74, 0x69, 0x61, 0x74, 0x6f, 0x72, 0x12, 0x24, 0x0a, 0x07, 0x4f, 0x64, 0x64, 0x49, 0x6e,
	0x66, 0x6f, 0x18, 0x0c, 0x20, 0x01, 0x28, 0x0b, 0x32, 0x0a, 0x2e, 0x66, 0x6f, 0x72, 0x6b, 0x73,
	0x2e, 0x4f, 0x64, 0x64, 0x52, 0x07, 0x4f, 0x64, 0x64, 0x49, 0x6e, 0x66, 0x6f, 0x22, 0x71, 0x0a,
	0x03, 0x4f, 0x64, 0x64, 0x12, 0x12, 0x0a, 0x04, 0x54, 0x79, 0x70, 0x65, 0x18, 0x01, 0x20, 0x01,
	0x28, 0x09, 0x52, 0x04, 0x54, 0x79, 0x70, 0x65, 0x12, 0x10, 0x0a, 0x03, 0x4d, 0x6f, 0x64, 0x18,
	0x02, 0x20, 0x01, 0x28, 0x09, 0x52, 0x03, 0x4d, 0x6f, 0x64, 0x12, 0x14, 0x0a, 0x05, 0x50, 0x61,
	0x72, 0x61, 0x6d, 0x18, 0x03, 0x20, 0x01, 0x28, 0x02, 0x52, 0x05, 0x50, 0x61, 0x72, 0x61, 0x6d,
	0x12, 0x12, 0x0a, 0x04, 0x43, 0x6f, 0x65, 0x66, 0x18, 0x04, 0x20, 0x01, 0x28, 0x02, 0x52, 0x04,
	0x43, 0x6f, 0x65, 0x66, 0x12, 0x1a, 0x0a, 0x08, 0x53, 0x65, 0x6c, 0x65, 0x63, 0x74, 0x6f, 0x72,
	0x18, 0x05, 0x20, 0x01, 0x28, 0x09, 0x52, 0x08, 0x53, 0x65, 0x6c, 0x65, 0x63, 0x74, 0x6f, 0x72,
	0x32, 0x98, 0x01, 0x0a, 0x0c, 0x46, 0x6f, 0x72, 0x6b, 0x73, 0x53, 0x65, 0x72, 0x76, 0x69, 0x63,
	0x65, 0x12, 0x3f, 0x0a, 0x0a, 0x53, 0x74, 0x6f, 0x72, 0x65, 0x46, 0x6f, 0x72, 0x6b, 0x73, 0x12,
	0x17, 0x2e, 0x66, 0x6f, 0x72, 0x6b, 0x73, 0x2e, 0x53, 0x74, 0x6f, 0x72, 0x65, 0x46, 0x6f, 0x72,
	0x6b, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x1a, 0x14, 0x2e, 0x66, 0x6f, 0x72, 0x6b, 0x73,
	0x2e, 0x46, 0x6f, 0x72, 0x6b, 0x73, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x22, 0x00,
	0x28, 0x01, 0x12, 0x47, 0x0a, 0x0f, 0x44, 0x69, 0x73, 0x74, 0x72, 0x69, 0x62, 0x75, 0x74, 0x65,
	0x46, 0x6f, 0x72, 0x6b, 0x73, 0x12, 0x17, 0x2e, 0x66, 0x6f, 0x72, 0x6b, 0x73, 0x2e, 0x53, 0x75,
	0x62, 0x73, 0x63, 0x72, 0x69, 0x62, 0x65, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x1a, 0x17,
	0x2e, 0x66, 0x6f, 0x72, 0x6b, 0x73, 0x2e, 0x44, 0x69, 0x73, 0x74, 0x72, 0x69, 0x62, 0x75, 0x74,
	0x65, 0x64, 0x46, 0x6f, 0x72, 0x6b, 0x73, 0x22, 0x00, 0x30, 0x01, 0x42, 0x1b, 0x5a, 0x19, 0x67,
	0x69, 0x74, 0x6c, 0x61, 0x62, 0x2e, 0x63, 0x6f, 0x6d, 0x2f, 0x6d, 0x65, 0x64, 0x65, 0x76, 0x6f,
	0x70, 0x73, 0x2f, 0x66, 0x6f, 0x72, 0x6b, 0x73, 0x62, 0x06, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x33,
}

var (
	file_forks_proto_rawDescOnce sync.Once
	file_forks_proto_rawDescData = file_forks_proto_rawDesc
)

func file_forks_proto_rawDescGZIP() []byte {
	file_forks_proto_rawDescOnce.Do(func() {
		file_forks_proto_rawDescData = protoimpl.X.CompressGZIP(file_forks_proto_rawDescData)
	})
	return file_forks_proto_rawDescData
}

var file_forks_proto_msgTypes = make([]protoimpl.MessageInfo, 7)
var file_forks_proto_goTypes = []interface{}{
	(*StoreForkRequest)(nil), // 0: forks.StoreForkRequest
	(*ForksResponse)(nil),    // 1: forks.ForksResponse
	(*SubscribeRequest)(nil), // 2: forks.SubscribeRequest
	(*DistributedForks)(nil), // 3: forks.DistributedForks
	(*Fork)(nil),             // 4: forks.Fork
	(*Shoulder)(nil),         // 5: forks.Shoulder
	(*Odd)(nil),              // 6: forks.Odd
}
var file_forks_proto_depIdxs = []int32{
	4, // 0: forks.StoreForkRequest.Forks:type_name -> forks.Fork
	4, // 1: forks.DistributedForks.Forks:type_name -> forks.Fork
	5, // 2: forks.Fork.FirstShoulder:type_name -> forks.Shoulder
	5, // 3: forks.Fork.SecondShoulder:type_name -> forks.Shoulder
	6, // 4: forks.Shoulder.OddInfo:type_name -> forks.Odd
	0, // 5: forks.ForksService.StoreForks:input_type -> forks.StoreForkRequest
	2, // 6: forks.ForksService.DistributeForks:input_type -> forks.SubscribeRequest
	1, // 7: forks.ForksService.StoreForks:output_type -> forks.ForksResponse
	3, // 8: forks.ForksService.DistributeForks:output_type -> forks.DistributedForks
	7, // [7:9] is the sub-list for method output_type
	5, // [5:7] is the sub-list for method input_type
	5, // [5:5] is the sub-list for extension type_name
	5, // [5:5] is the sub-list for extension extendee
	0, // [0:5] is the sub-list for field type_name
}

func init() { file_forks_proto_init() }
func file_forks_proto_init() {
	if File_forks_proto != nil {
		return
	}
	if !protoimpl.UnsafeEnabled {
		file_forks_proto_msgTypes[0].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*StoreForkRequest); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_forks_proto_msgTypes[1].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*ForksResponse); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_forks_proto_msgTypes[2].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*SubscribeRequest); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_forks_proto_msgTypes[3].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*DistributedForks); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_forks_proto_msgTypes[4].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*Fork); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_forks_proto_msgTypes[5].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*Shoulder); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_forks_proto_msgTypes[6].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*Odd); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
	}
	type x struct{}
	out := protoimpl.TypeBuilder{
		File: protoimpl.DescBuilder{
			GoPackagePath: reflect.TypeOf(x{}).PkgPath(),
			RawDescriptor: file_forks_proto_rawDesc,
			NumEnums:      0,
			NumMessages:   7,
			NumExtensions: 0,
			NumServices:   1,
		},
		GoTypes:           file_forks_proto_goTypes,
		DependencyIndexes: file_forks_proto_depIdxs,
		MessageInfos:      file_forks_proto_msgTypes,
	}.Build()
	File_forks_proto = out.File
	file_forks_proto_rawDesc = nil
	file_forks_proto_goTypes = nil
	file_forks_proto_depIdxs = nil
}
